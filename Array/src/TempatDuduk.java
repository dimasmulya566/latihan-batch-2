import java.util.Scanner;

public class TempatDuduk {
    public static void main(String[] args) {
        String[][] Kursi = new String[5][5];
        Scanner scan = new Scanner(System.in);

        for(int bar = 0; bar < Kursi.length; bar++){
            for(int kol = 0; kol < Kursi[bar].length; kol++){
                System.out.format("Kursi nomor berapa ini (d%,d%): " ,bar, kol);
                Kursi[bar][kol] = scan.nextLine();
            }
        }
        System.out.println("--------------------------");
        for(int bar = 0; bar < Kursi.length; bar++) {
            for (int kol = 0; kol < Kursi[bar].length; kol++) {
                System.out.format("| %s | \t", Kursi[bar][kol]);
            }
            System.out.println("");
        }
        System.out.println("-------------------------");
    }
}
