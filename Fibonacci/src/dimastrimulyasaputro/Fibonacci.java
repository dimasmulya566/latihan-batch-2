package dimastrimulyasaputro;

import java.util.Scanner;

public class Fibonacci {

    public static void main(String[] args) {
	// write your code here
        showFibonacci(5);
    }

    public static void showFibonacci(int input) {
        int fibonacciLimit, fibonacciSebelum, fibonacciAwal, fibonacciAkhir;

        Scanner inputUser = new Scanner(System.in);
        System.out.println("Mengambil nilai fibonacci ke - :");
        fibonacciAkhir = inputUser.nextInt();

        fibonacciAwal = 1;
        fibonacciSebelum = 1;
        fibonacciLimit = 1;


        for (int i = 0; i <= fibonacciAkhir; i++) {
            System.out.println("nilai ke - " + i + " adalah " + fibonacciLimit);
            fibonacciLimit = fibonacciSebelum + fibonacciAwal;
            fibonacciSebelum = fibonacciAwal;
            fibonacciAwal = fibonacciLimit;
        }
    }
}
