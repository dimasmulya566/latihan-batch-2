package com.belajar;

public class AlatMusic {

	private String jenis;
	private int harga;
	
	
	public AlatMusic() {
		super();
	}
	
	public AlatMusic(String jenis, int harga) {
		super();
		this.jenis = jenis;
		this.harga = harga;
	}
	
	
	public String getJenis() {
		return jenis;
	}
	public void setJenis(String jenis) {
		this.jenis = jenis;
	}
	public int getHarga() {
		return harga;
	}
	public void setHarga(int harga) {
		this.harga = harga;
	}
	
	
//	overriding
	public int getTotal(int harga) {
		return harga * 2;
	}
	
//	overloading
	public int getTotal(String jenis, int harga) {
		return harga +2;
	}

	@Override
	public String toString() {
		return " Harga alat musik : " + getTotal(harga);
	}
	
	
	
}
