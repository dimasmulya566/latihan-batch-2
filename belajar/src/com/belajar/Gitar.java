package com.belajar;

public class Gitar extends AlatMusic {

	
	private String merk;

	
	
	public Gitar() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Gitar(String jenis, int harga) {
		super(jenis, harga);
		// TODO Auto-generated constructor stub
	}
	

	public Gitar(String merk) {
		super();
		this.merk = merk;
	}

	public String getMerk() {
		return merk;
	}

	public void setMerk(String merk) {
		this.merk = merk;
	}

	@Override
	public int getTotal(int harga) {
		return harga * 10 /2;
	}
	
	
	
	
	
	
}
