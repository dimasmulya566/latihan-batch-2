package com.dimasapril;

class Player{
    String nama;
    Double darah;
    // membuat object member
    Senjata senjata;
    Armor armor;

    Player(String nama, Double darah){
        this.nama = nama;
        this.darah = darah;
    }

    void equipSenjata(Senjata senjata){
        this.senjata = senjata;
    }
}

class Senjata{
    Double senjata;
    String nama;

    Senjata(String nama, Double senjata){
        this.senjata = senjata;
        this.nama = nama;
    }

    void display(){
        System.out.println("Senjata  : " + this.nama + "Power : " + this.senjata);
    }
}

class Armor{
    Double ketebalan;

    Armor(Double ketebalan){
        this.ketebalan = ketebalan;
    }
}

public class Main {

    public static void main(String[] args) {
	// write your code here
        Player player1 = new Player("Deni",100);
        Senjata pistol = new Senjata("Pistol",63);
        Armor perisai = new Armor(43);

        player1.equipSenjata(pistol);
        player1.senjata.display();
    }
}
