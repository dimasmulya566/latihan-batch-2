package dimastrimulyasaputro.thread;

public class MainThread {
    public static void main(String[] args) {
        String namaThreadUtama = "Agustina";
        System.out.println("Kasir Pertama : " + namaThreadUtama);

        Thread KasirBaru = new Thread(new KasirBaru());
        KasirBaru.start();

        for(int pembeli = 1; pembeli <= 6; pembeli++) {
            System.out.println(namaThreadUtama + " melayani pembeli nomor " + pembeli);
        }
    }

}
