package dimastrimulyasaputro.thread.threadPool;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class Hardware {

    public static void main(String[] args) {
        ExecutorService service = Executors.newFixedThreadPool(2);

        for(int i=0; i < 30; i++){
            service.submit(new Komputer(i));
        }

        service.shutdown();
        System.out.println("Semua angka telah dihitung");

        try{
            service.awaitTermination(1, TimeUnit.DAYS);
        } catch (InterruptedException e){
            e.printStackTrace();
        }

        System.out.println("Telah Selesai dihitung");

    }
}
