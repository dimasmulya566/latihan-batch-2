package dimastrimulyasaputro.thread.threadPool;

import  java.util.concurrent.ExecutorService;
import  java.util.concurrent.Executors;
import  java.util.concurrent.TimeUnit;

public class Komputer implements Runnable{

    private int id;

    public Komputer(int id){
        this.id = id;
    }

    @Override
    public void run() {
        System.out.println("Memulai dari = angka " + id);

        try{
            Thread.sleep(1000);
        } catch(InterruptedException e){
            e.printStackTrace();
        }

        System.out.println("Telah berakhir menghitung " + id);
    }
}
