class Player(){
    String nama;
    Double darah;
    // membuat object member
    Senjata senjata;
    Armor armor;

    Player(String nama, Double darah){
        this.nama = nama;
        this.darah = darah;
    }

    void equipSenjata(Senjata senjata){
        this.senjata = senjata;
    }
}

class Senjata(){
    Double senjata;

    Senjata(Double senjata){
        this.senjata = senjata;
    }
}

class Armor(){
    Double ketebalan;

    Armor(Double ketebalan){
        this.ketebalan = ketebalan;
    }
}

public class Belajar {
    public static void main(String[] args) {
        Player player1 = new Player(Dimas,100);
        Senjata pistol = new Senjata(88);
        Armor bajuBesi = new Armor(43);

        player1.equipSenjata(pistol);
    }
}