package com.example.dimastrimulyasaputro.tugas_lombok_dan_transactional.Controller;

import com.example.dimastrimulyasaputro.tugas_lombok_dan_transactional.Model.UserBook;
import com.example.dimastrimulyasaputro.tugas_lombok_dan_transactional.Repository.UserService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RequestMapping("/user")
@RestController
public class BookController {

    @Autowired
    private UserService userService;

    @GetMapping
    public Iterable<UserBook> getAll(){
        return userService.findAll();
    }

    @PostMapping
    public String create(@RequestBody UserBook userBook){
        return userService.create(userBook);
    }

    @PutMapping("{id}/{nominal}")
    public UserBook update(@PathVariable("id") String id, @PathVariable("nominal") int nominal){
        return userService.update(id, nominal);
    }
}
