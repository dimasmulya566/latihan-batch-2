package com.example.dimastrimulyasaputro.tugas_lombok_dan_transactional.Model;

import lombok.*;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.ManyToAny;

import javax.persistence.*;

@Entity(name = "save_account")
@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class AcountSaving {
    @Id
    @Column(name = "id", nullable = false, length = 64)
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid2")
    private String id;

    @Column(name = "nominal", nullable = false)
    private int nominal;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private UserBook userBook;
}
