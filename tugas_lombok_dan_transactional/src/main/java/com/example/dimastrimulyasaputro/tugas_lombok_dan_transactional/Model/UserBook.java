package com.example.dimastrimulyasaputro.tugas_lombok_dan_transactional.Model;

import lombok.*;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Data
@Entity(name = "user_id")
@NoArgsConstructor
@AllArgsConstructor
public class UserBook {

    @Id
    @GeneratedValue(generator = "master-book")
    @GenericGenerator(name = "master-book", strategy = "uuid2")
    @Column(name = "id", nullable = false,length = 64)
    private String Id;

    @Column(name = "name", nullable = false,length = 100)
    private String Name;

    @Column(name = "address", nullable = false,length = 100)
    private String Address;

    @Column(name = "nominal", nullable = false) // penamaan dicolumn harus kecil semua
    private int nominal;

}
