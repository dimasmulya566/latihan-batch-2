package com.example.dimastrimulyasaputro.tugas_lombok_dan_transactional.Repository;

import com.example.dimastrimulyasaputro.tugas_lombok_dan_transactional.Model.AcountSaving;
import com.example.dimastrimulyasaputro.tugas_lombok_dan_transactional.Model.UserBook;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;


@Service
@AllArgsConstructor
public class SaveusAccountService {

  @Autowired
  private SaveusRepository saveusRepository;


    @Transactional(propagation = Propagation.REQUIRED)
    public AcountSaving save(UserBook userBook,int Nominal){
        AcountSaving acountSavingBuild = AcountSaving.builder()
                .nominal(Nominal)
                .userBook(userBook)
                .build();
        AcountSaving acountSavingSave = saveusRepository.save(acountSavingBuild);
        return acountSavingSave;
    }

}
