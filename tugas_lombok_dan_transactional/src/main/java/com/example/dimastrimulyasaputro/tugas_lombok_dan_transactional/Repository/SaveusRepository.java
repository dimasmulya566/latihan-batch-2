package com.example.dimastrimulyasaputro.tugas_lombok_dan_transactional.Repository;

import com.example.dimastrimulyasaputro.tugas_lombok_dan_transactional.Model.AcountSaving;
import org.springframework.data.repository.CrudRepository;

public interface SaveusRepository extends CrudRepository<AcountSaving, String> {
}
