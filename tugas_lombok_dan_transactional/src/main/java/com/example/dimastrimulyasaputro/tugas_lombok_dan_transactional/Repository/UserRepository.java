package com.example.dimastrimulyasaputro.tugas_lombok_dan_transactional.Repository;

import com.example.dimastrimulyasaputro.tugas_lombok_dan_transactional.Model.UserBook;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<UserBook, String> {
}
