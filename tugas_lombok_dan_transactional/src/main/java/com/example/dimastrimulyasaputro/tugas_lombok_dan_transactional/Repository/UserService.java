package com.example.dimastrimulyasaputro.tugas_lombok_dan_transactional.Repository;

import com.example.dimastrimulyasaputro.tugas_lombok_dan_transactional.Model.AcountSaving;
import com.example.dimastrimulyasaputro.tugas_lombok_dan_transactional.Model.UserBook;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;

import javax.transaction.Transactional;

@Service
@Slf4j
@AllArgsConstructor
public class UserService {
    private UserRepository userRepository;
    private  SaveusAccountService saveusAccountService;

    @Transactional
    public String create(UserBook userBook){
        UserBook userSave = userRepository.save(userBook);
        log.info("user save {} {} ",userSave,"SUCCESS");
        AcountSaving acountSaving = saveusAccountService.save(userSave, userSave.getNominal());
        log.info("Save Account Success {}",acountSaving);
        return "Success";
    }

    @org.springframework.transaction.annotation.Transactional(isolation = Isolation.READ_UNCOMMITTED)
    public Iterable<UserBook> findAll() {
        var userSave = userRepository.findAll();
        log.info("user save {} ",userSave);
        return userSave;
    }

    @org.springframework.transaction.annotation.Transactional(isolation = Isolation.REPEATABLE_READ)
    public UserBook update(String id, int nominal) {
        var userFind = userRepository.findById(id)
                .stream()
                .peek(user -> user.setNominal(user.getNominal() + nominal) )
                .findFirst().orElseThrow();
        UserBook userSave = userRepository.save(userFind);
        System.out.println(userSave);
        return userSave;
    }

}
