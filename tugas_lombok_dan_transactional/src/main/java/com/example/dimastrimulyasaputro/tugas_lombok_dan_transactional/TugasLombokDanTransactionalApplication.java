package com.example.dimastrimulyasaputro.tugas_lombok_dan_transactional;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TugasLombokDanTransactionalApplication {

	public static void main(String[] args) {
		SpringApplication.run(TugasLombokDanTransactionalApplication.class, args);
	}

}
